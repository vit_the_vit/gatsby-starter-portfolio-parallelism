module.exports = {
  siteMetadata: {
    title: "Gatsby Starter - Parallelism ",
    author: "Vitaly Kolesnik",
    description: "A Gatsby.js v2 portfolio starter based on HTML5 UP Parallelism theme"
  },
  plugins: [
    'gatsby-plugin-react-helmet',
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: 'gatsby-starter-default',
        short_name: 'starter',
        start_url: '/',
        background_color: '#d64760',
        theme_color: '#ffffff',
        display: 'minimal-ui',
        icon: 'src/images/gatsby-icon.png', // This path is relative to the root of the site.
      },
    },
    'gatsby-plugin-sass',
    'gatsby-plugin-offline'
  ],
}
