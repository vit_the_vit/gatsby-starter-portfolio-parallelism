# gatsby-starter-portfolio-parallelism

**This is a portfolio starter with horizontal scrolling for Gatsby.js V2.**

## Preview

https://gatsby-starter-parallelism.netlify.com

## Installation

Install this starter (assuming Gatsby is installed) by running from your CLI:

`gatsby new parallelism https://gitlab.com/vit.kolesnik/gatsby-starter-portfolio-parallelism`

Run `gatsby develop` in the terminal to start the dev site.

Please note: the images are not part of the template and are used for demonstration purposes only.
