import React from 'react';

const Footer = (props) => (

    <section id="footer">
        <section>
          <p><strong>Parallelism</strong> Gatsby Starter, a responsive portfolio template with horizontal scrolling using an <a href="https://html5up.net/">HTML5 UP</a> theme. 
          Free for personal and commercial use under the <a href="https://html5up.net/license">Creative Commons Attribution</a> license.</p>
        </section>
        <section>
          <ul className="icons">
            <li><a href="#" className="icon brands fa-twitter"><span className="label">Twitter</span></a></li>
            <li><a href="#" className="icon brands fa-instagram"><span className="label">Instagram</span></a></li>
            <li><a href="#" className="icon brands fa-facebook-f"><span className="label">Facebook</span></a></li>
            <li><a href="#" className="icon brands fa-dribbble"><span className="label">Dribbble</span></a></li>
            <li><a href="#" className="icon solid fa-envelope"><span className="label">Email</span></a></li>
          </ul>
          <ul className="copyright">
            <li>Built with: <a href="https://www.gatsbyjs.org/">Gatsby.js</a></li>
            <li>Design: <a href="http://html5up.net/">HTML5 UP</a></li>
            <li>Gatsby Port: <a href="https://vitkolesnik.com/">Vitaly Kolesnik</a> </li>
          </ul>
      </section>
    </section>
)

export default Footer
