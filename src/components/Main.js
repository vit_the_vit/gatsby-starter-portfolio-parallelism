import React from 'react'
import { Link } from 'gatsby'

import pic01t from '../images/400-01.jpg'
import pic02t from '../images/400-02.jpg'
import pic03t from '../images/400-03.jpg'
import pic04t from '../images/400-04.jpg'
import pic05t from '../images/400-05.jpg'
import pic06t from '../images/400-06.jpg'
import pic07t from '../images/400-07.jpg'
import pic08t from '../images/400-08.jpg'
import pic09t from '../images/400-09.jpg'
import pic10t from '../images/400-10.jpg'
import pic11t from '../images/400-11.jpg'
import pic12t from '../images/400-12.jpg'

class Main extends React.Component {
  render() {

    return (
      <section id="main" className="container">
        <div className="items horizontal-scroll-wrapper">
          <div className="item intro span-2">
            <h1>Parallelism</h1>
            <p>A responsive portfolio site<br />
            template by HTML5 UP</p>
          </div>
          <section className="item thumb span-1">
            <h2>One</h2>
            <Link to="/page-01/"><div className="image"><img src={pic01t} alt="" /></div></Link>
          </section>
          <section className="item thumb span-2">
            <h2>Two</h2>
            <Link to="/page-02/"><div className="image"><img src={pic02t} alt="" /></div></Link>
          </section>
          <section className="item thumb span-3">
            <h2>Three</h2>
            <Link to="/page-03/"><div className="image"><img src={pic03t} alt="" /></div></Link>
          </section>
          <section className="item thumb span-2">
            <h2>Four</h2>
            <Link to="/page-04/"><div className="image"><img src={pic04t} alt="" /></div></Link>
          </section>
          <section className="item thumb span-1">
            <h2>Five</h2>
            <Link to="/page-05/"><div className="image"><img src={pic05t} alt="" /></div></Link>
          </section>
          <section className="item thumb span-2">
            <h2>Six</h2>
            <Link to="/page-06/"><div className="image"><img src={pic06t} alt="" /></div></Link>
          </section>
        </div>
        <div className="items horizontal-scroll-wrapper">
          <section className="item thumb span-2">
            <h2>Seven</h2>
            <Link to="/page-07/"><div className="image"><img src={pic07t} alt="" /></div></Link>
          </section>
          <section className="item thumb span-1">
            <h2>Eight</h2>
            <Link to="/page-08/"><div className="image"><img src={pic08t} alt="" /></div></Link>
          </section>
          <section className="item thumb span-3">
            <h2>Nine</h2>
            <Link to="/page-09/"><div className="image"><img src={pic09t} alt="" /></div></Link>
          </section>
          <section className="item thumb span-1">
            <h2>Ten</h2>
            <Link to="/page-10/"><div className="image"><img src={pic10t} alt="" /></div></Link>
          </section>
          <section className="item thumb span-2">
            <h2>Eleven</h2>
            <Link to="/page-11/"><div className="image"><img src={pic11t} alt="" /></div></Link>
          </section>
          <section className="item thumb span-3">
            <h2>Twelve</h2>
            <Link to="/page-12/"><div className="image"><img src={pic12t} alt="" /></div></Link>
          </section>
        </div>
      </section>
    )
  }
}

export default Main
