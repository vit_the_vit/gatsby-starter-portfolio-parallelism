import React from 'react'
import Layout from '../components/layout'
import Header from '../components/Header'
import Main from '../components/Main'
import Footer from '../components/Footer'

class IndexPage extends React.Component {

  componentDidMount () {
    let xScrollable = document.querySelectorAll('.horizontal-scroll-wrapper');
    for (let item of xScrollable) {
      document.addEventListener('wheel', (e) => {
        if (e.deltaY > 0) item.scrollLeft += 100;
        else item.scrollLeft -= 100;
      });
    }
  }

  render() {
    return (
      <Layout location={this.props.location}>
          <div id="wrapper">
            <Header />
            <Main />
            <Footer />
          </div>
          <div id="bg"></div>
      </Layout>
    )
  }
}

export default IndexPage
