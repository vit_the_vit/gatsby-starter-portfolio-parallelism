import React from 'react'
import { Link } from 'gatsby'

import Layout from '../components/layout'
import pic04f from '../images/800-04.jpg'

const Page04 = () => (
  <Layout>
    <Link to="/" className="home-link">Home</Link>

    <article
      id="four"
      className="single"
    >
      <h1>Four</h1>
      <img src={pic04f} alt="" />
      <span className="caption">Photo by <a href="https://unsplash.com/photos/rewyZqUwAqY">Jonathan Pendleton</a> on Unsplash</span>
      <p>
      Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
      </p>
    </article>

  </Layout>
)

export default Page04